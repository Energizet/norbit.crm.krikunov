﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Norbit.Crm.Krikunov.Task4
{
	class Program
	{
		static void Main(string[] args)
		{
			var array = new[] { 5, 9, 8, 3, 1, 6, 2, 4, 7 };
			PrintArray(array);

			var bubbleSort = new BubbleSort<int>();
			bubbleSort.IsSortedEventHandler += () => Console.WriteLine("Массив отсортирован");
			bubbleSort.SortedEventHandler += sortedArray =>
			{
				Console.WriteLine("Отсортированый массив из события");
				PrintArray(sortedArray);
			};
			var newArray = bubbleSort.Sort(array, (l, r) => l > r ? 1 : 0);
			Console.WriteLine("Отсортированый массив возвращённый");
			PrintArray(newArray);
		}

		/// <summary>
		/// Печатает массив
		/// </summary>
		static void PrintArray<T>(IEnumerable<T> array)
		{
			Console.WriteLine(string.Join(", ", array));
		}
	}
}
