﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Norbit.Crm.Krikunov.Task4
{
	/// <summary>
	/// Пузырьковая сортировка
	/// </summary>
	public class BubbleSort<T>
	{
		/// <summary>
		/// Событие завершения сортировки и выводит последовательность
		/// </summary>
		public event Action<IEnumerable<T>> SortedEventHandler;
		/// <summary>
		/// Событие завершения сортировки
		/// </summary>
		public event Action IsSortedEventHandler;

		/// <summary>
		/// Делегат сравнения левого и правого элементов
		/// </summary>
		/// <param name="l">Левый (первый) элемент</param>
		/// <param name="r">Правый (второй) элемент</param>
		/// <returns>1 - если левый элемент после второго,
		/// 0 - если элементы равны
		/// -1 - если левый раньше второго</returns>
		public delegate int CompareCallback(T l, T r);

		/// <summary>
		/// Сортирует перечисление по указаному сравнению
		/// </summary>
		/// <param name="array">Перечисление</param>
		/// <param name="callback">Проверка сортровки</param>
		/// <returns>Отсортированое перечисление</returns>
		public IEnumerable<T> Sort(IEnumerable<T> array, CompareCallback callback)
		{
			var list = array.ToList();
			for (var i = 0; i < list.Count; i++)
			{
				for (var j = 1; j < list.Count - i; j++)
				{
					if (callback(list[j - 1], list[j]) > 0)
					{
						var tmp = list[j];
						list[j] = list[j - 1];
						list[j - 1] = tmp;
					}
				}
			}

			IsSortedEventHandler?.Invoke();
			SortedEventHandler?.Invoke(list);
			return list;
		}
	}
}