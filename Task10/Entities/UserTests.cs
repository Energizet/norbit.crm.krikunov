﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task10.Entities
{
	/// <summary>
	/// Тестирование пользователя.
	/// </summary>
	[TestClass]
	public class UserTests
	{
		/// <summary>
		/// Создание дефолтного пользователя.
		/// </summary>
		[TestMethod]
		public void UserTest()
		{
			var user = new User();
			Assert.IsTrue(true);
		}

		/// <summary>
		/// ПОльзователяь с параметрами.
		/// </summary>
		[TestMethod]
		public void UserSuccessTest()
		{
			var birthDay = new DateTime(2000, 1, 1);
			var user = new User("last", "first", "middle", birthDay);
			Assert.IsTrue(true);
		}

		/// <summary>
		/// ПОльзователяь с ексепшеном в параметрах.
		/// </summary>
		[ExpectedException(typeof(ArgumentException))]
		[TestMethod]
		public void UserFailTest()
		{
			var birthDay = new DateTime(DateTime.Now.Year + 1, 1, 1);
			var user = new User("last", "first", "middle", birthDay);
		}

		/// <summary>
		/// ToString пользователя
		/// </summary>
		[TestMethod]
		public void ToStringTest()
		{
			var birthDay = new DateTime(2000, 1, 1);
			var age = (int)(DateTime.Now - birthDay).TotalDays / 365;
			var user = new User("last", "first", "middle", birthDay);
			Assert.AreEqual($"Фамилия = last\nИмя = first\nОтчество = middle\nДень рождения = 01.01.2000\nВозраст = {age}", user.ToString());
		}
	}
}