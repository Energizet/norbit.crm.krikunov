﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task10.Entities
{
	/// <summary>
	/// Тестирование сотрудника.
	/// </summary>
	[TestClass]
	public class EmployeeTests
	{
		/// <summary>
		/// Создание дефолтного сотрудника.
		/// </summary>
		[TestMethod]
		public void EmployeeTest()
		{
			var employee = new Employee();
			Assert.IsTrue(true);
		}

		/// <summary>
		/// Сотрудника с параметрами.
		/// </summary>
		[TestMethod]
		public void EmployeeSuccessTest()
		{
			var birthDay = new DateTime(2000, 1, 1);
			var employee = new Employee("last", "first", "middle", birthDay, 1, "position");
			Assert.IsTrue(true);
		}

		/// <summary>
		/// Сотрудника с ексепшеном в параметрах.
		/// </summary>
		[DataRow(3000, 1)]
		[DataRow(2000, -1)]
		[ExpectedException(typeof(ArgumentException))]
		[TestMethod]
		public void EmployeeFailTest(int year, int experience)
		{
			var birthDay = new DateTime(year, 1, 1);
			var employee = new Employee("last", "first", "middle", birthDay, experience, "position");
		}

		/// <summary>
		/// ToString сотрудника
		/// </summary>
		[TestMethod]
		public void ToStringTest()
		{
			var birthDay = new DateTime(2000, 1, 1);
			var age = (int)(DateTime.Now - birthDay).TotalDays / 365;
			var employee = new Employee("last", "first", "middle", birthDay, 1, "position");
			Assert.AreEqual($"Фамилия = last\nИмя = first\nОтчество = middle\nДень рождения = 01.01.2000\nВозраст = {age}\nСтаж работы = 1\nДолжность = position", employee.ToString());
		}
	}
}