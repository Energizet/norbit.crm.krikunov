﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task10.Entities
{
	/// <summary>
	/// Тестирование круга.
	/// </summary>
	[TestClass]
	public class RoundTests
	{
		/// <summary>
		/// Правельный круг.
		/// </summary>
		[TestMethod]
		public void RoundSuccessTest()
		{
			var round = new Round(1, new Point());
		}

		/// <summary>
		/// Падающий круг.
		/// </summary>
		[ExpectedException(typeof(ArgumentException))]
		[TestMethod]
		public void RoundFailTest1()
		{
			var round = new Round(-1, new Point());
		}
	}
}