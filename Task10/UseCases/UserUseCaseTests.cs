﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Norbit.Crm.Krikunov.Task7.Controllers.UserController;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;
using Norbit.Crm.Krikunov.Task7.UseCases;
using NSubstitute;

namespace Norbit.Crm.Krikunov.Task10.UseCases
{
	/// <summary>
	/// Use case пользователя.
	/// </summary>
	[TestClass]
	public class UserUseCaseTests
	{
		private User _user;
		private int _exceptionCount;

		/// <summary>
		/// Удачный сценарий.
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="year">Год рождения</param>
		[DataRow("last", "first", "middle", 2000)]
		[TestMethod]
		public async Task GetAndPrintUserSuccessTest(string lastName, string firstName, string middleName, int year)
		{
			var birthDay = new DateTime(year, 1, 1);
			var age = (int)(DateTime.Now - birthDay).TotalDays / 365;

			var isSuccess = false;
			await InitMock(lastName, firstName, middleName, birthDay,
				async (controller, repository) =>
				{
					var roundUseCase = new UserUseCase(controller, repository);
					await roundUseCase.GetAndPrintUser();
					Assert.IsTrue(_exceptionCount <= 0);
					Assert.IsNotNull(_user);
					Assert.AreEqual($"Фамилия = {lastName}\nИмя = {firstName}\nОтчество = {middleName}\nДень рождения = 01.01.{year}\nВозраст = {age}", _user.ToString());
					isSuccess = true;
				});
			if (!isSuccess)
			{
				Assert.Fail();
			}
		}

		/// <summary>
		/// Провальный сценарий.
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="year">Год рождения</param>
		[DataRow("last", "first", "middle", 3000)]
		[TestMethod]
		public async Task GetAndPrintUserFailTest(string lastName, string firstName, string middleName, int year)
		{
			var birthDay = new DateTime(year, 1, 1);

			var isSuccess = false;
			await InitMock(lastName, firstName, middleName, birthDay,
				async (controller, repository) =>
				{
					var roundUseCase = new UserUseCase(controller, repository);
					await roundUseCase.GetAndPrintUser();
					Assert.IsTrue(_exceptionCount > 0);
					Assert.IsNull(_user);
					isSuccess = true;
				});
			if (!isSuccess)
			{
				Assert.Fail();
			}
		}

		/// <summary>
		/// Инициализация контроллера и репозитория
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="year">Год рождения</param>
		/// <param name="callback">Колбек</param>
		private async Task InitMock(string lastName, string firstName, string middleName, DateTime birthDay, Func<IUserController, IUserRepository, Task> callback)
		{
			var controller = Substitute.For<IUserController>();
			controller.When(x => x.PrintUser(Arg.Any<User>()))
				.Do(callInfo => _user = callInfo[0] as User);
			controller.When(x => x.PrintException(Arg.Any<Exception>(), Arg.Any<bool>()))
				.Do(callInfo => _exceptionCount++);

			var repository = Substitute.For<IUserRepository>();
			repository.GetLastName().Returns(Task.Run(() => lastName));
			repository.GetFirstName().Returns(Task.Run(() => firstName));
			repository.GetMiddleName().Returns(Task.Run(() => middleName));
			repository.GetBirthDay().Returns(Task.Run(() => birthDay));

			await callback(controller, repository);
		}
	}
}