﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Norbit.Crm.Krikunov.Task7.Controllers.RoundController;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;
using Norbit.Crm.Krikunov.Task7.UseCases;
using NSubstitute;

namespace Norbit.Crm.Krikunov.Task10.UseCases
{
	/// <summary>
	/// Use Case круга.
	/// </summary>
	[TestClass]
	public class RoundUseCaseTests
	{
		private Round _round;
		private int _exceptionCount;

		/// <summary>
		/// Удачный сценарий.
		/// </summary>
		/// <param name="radius">Радиус</param>
		[DataRow(10)]
		[TestMethod]
		public async Task GetAndPrintRoundSuccessTest(double radius)
		{
			var isSuccess = false;
			await InitMock(radius, async (controller, repository) =>
			{
				var roundUseCase = new RoundUseCase(controller, repository);
				await roundUseCase.GetAndPrintRound();
				Assert.IsTrue(_exceptionCount <= 0);
				Assert.IsNotNull(_round);
				Assert.AreEqual(314.1592653589793, _round.S);
				isSuccess = true;
			});
			if (!isSuccess)
			{
				Assert.Fail();
			}
		}

		/// <summary>
		/// Провальный сценарий.
		/// </summary>
		/// <param name="radius">Радиус</param>
		[DataRow(-10)]
		[TestMethod]
		public async Task GetAndPrintRoundFailTest(double radius)
		{
			var isSuccess = false;
			await InitMock(radius, async (controller, repository) =>
			{
				var roundUseCase = new RoundUseCase(controller, repository);
				await roundUseCase.GetAndPrintRound();
				Assert.IsTrue(_exceptionCount > 0);
				Assert.IsNull(_round);
				isSuccess = true;
			});
			if (!isSuccess)
			{
				Assert.Fail();
			}
		}

		/// <summary>
		/// Инициализация контроллера и репозитория
		/// </summary>
		/// <param name="radius">Радиус</param>
		/// <param name="callback">Колбек</param>
		private async Task InitMock(double radius, Func<IRoundController, IRoundRepository, Task> callback)
		{
			var controller = Substitute.For<IRoundController>();
			controller.When(x => x.PrintRound(Arg.Any<Round>()))
				.Do(callInfo => _round = callInfo[0] as Round);
			controller.When(x => x.PrintException(Arg.Any<Exception>(), Arg.Any<bool>()))
				.Do(callInfo => _exceptionCount++);

			var repository = Substitute.For<IRoundRepository>();
			repository.GetRadius().Returns(Task.Run(() => radius));

			await callback(controller, repository);
		}
	}
}