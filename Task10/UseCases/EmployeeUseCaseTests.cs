﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Norbit.Crm.Krikunov.Task7.Controllers.EmployeeController;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;
using Norbit.Crm.Krikunov.Task7.UseCases;
using NSubstitute;

namespace Norbit.Crm.Krikunov.Task10.UseCases
{
	/// <summary>
	/// Use case сотрудника.
	/// </summary>
	[TestClass]
	public class EmployeeUseCaseTests
	{
		private Employee _employee;
		private int _exceptionCount;

		/// <summary>
		/// Удачный сценарий.
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="year">Год рождения</param>
		/// <param name="experience">Стаж работы</param>
		/// <param name="position">Должность</param>
		[DataRow("last", "first", "middle", 2000, 10, "position")]
		[TestMethod]
		public async Task GetAndPrintEmployeeSuccessTest(string lastName, string firstName, string middleName, int year, int experience, string position)
		{
			var birthDay = new DateTime(year, 1, 1);
			var age = (int)(DateTime.Now - birthDay).TotalDays / 365;

			var isSuccess = false;
			await InitMock(lastName, firstName, middleName, birthDay, experience, position,
				async (controller, repository) =>
				{
					var roundUseCase = new EmployeeUseCase(controller, repository);
					await roundUseCase.GetAndPrintEmployee();
					Assert.IsTrue(_exceptionCount <= 0);
					Assert.IsNotNull(_employee);
					Assert.AreEqual($"Фамилия = {lastName}\nИмя = {firstName}\nОтчество = {middleName}\nДень рождения = 01.01.{year}\nВозраст = {age}\nСтаж работы = {experience}\nДолжность = {position}", _employee.ToString());
					isSuccess = true;
				});
			if (!isSuccess)
			{
				Assert.Fail();
			}
		}

		/// <summary>
		/// Провальный сценарий.
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="year">Год рождения</param>
		/// <param name="experience">Стаж работы</param>
		/// <param name="position">Должность</param>
		[DataRow("last", "first", "middle", 3000, 10, "position")]
		[DataRow("last", "first", "middle", 2000, -10, "position")]
		[DataRow("last", "first", "middle", 3000, -10, "position")]
		[TestMethod]
		public async Task GetAndPrintEmployeeFailTest(string lastName, string firstName, string middleName, int year, int experience, string position)
		{
			var birthDay = new DateTime(year, 1, 1);

			var isSuccess = false;
			await InitMock(lastName, firstName, middleName, birthDay, experience, position,
				async (controller, repository) =>
				{
					var roundUseCase = new EmployeeUseCase(controller, repository);
					await roundUseCase.GetAndPrintEmployee();
					Assert.IsTrue(_exceptionCount > 0);
					Assert.IsNull(_employee);
					isSuccess = true;
				});
			if (!isSuccess)
			{
				Assert.Fail();
			}
		}

		/// <summary>
		/// Инициализация контроллера и репозитория
		/// </summary>
		/// <param name="lastName">Фамилия</param>
		/// <param name="firstName">Имя</param>
		/// <param name="middleName">Отчество</param>
		/// <param name="year">Год рождения</param>
		/// <param name="experience">Стаж работы</param>
		/// <param name="position">Должность</param>
		/// <param name="callback">Колбек</param>
		private async Task InitMock(string lastName, string firstName, string middleName, DateTime birthDay, int experience, string position, Func<IEmployeeController, IEmployeeRepository, Task> callback)
		{
			var controller = Substitute.For<IEmployeeController>();
			controller.When(x => x.PrintEmployee(Arg.Any<Employee>()))
				.Do(callInfo => _employee = callInfo[0] as Employee);
			controller.When(x => x.PrintException(Arg.Any<Exception>(), Arg.Any<bool>()))
				.Do(callInfo => _exceptionCount++);

			var repository = Substitute.For<IEmployeeRepository>();
			repository.GetLastName().Returns(Task.Run(() => lastName));
			repository.GetFirstName().Returns(Task.Run(() => firstName));
			repository.GetMiddleName().Returns(Task.Run(() => middleName));
			repository.GetBirthDay().Returns(Task.Run(() => birthDay));
			repository.GetExperience().Returns(Task.Run(() => experience));
			repository.GetPosition().Returns(Task.Run(() => position));

			await callback(controller, repository);
		}
	}
}