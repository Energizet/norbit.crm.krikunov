﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task11
{
	/// <summary>
	/// Обёртка над ADO.NET.
	/// </summary>
	public class Sql : IDisposable
	{
		/// <summary>
		/// Подкюление.
		/// </summary>
		private readonly SqlConnection _connection;

		/// <summary>
		/// Устанавливает строку подключения из App.config
		/// </summary>
		public Sql()
		{
			var connectionString = ConfigurationManager.AppSettings.Get("connection");
			_connection = new SqlConnection(connectionString);
		}

		/// <summary>
		/// Выборка
		/// </summary>
		/// <param name="sql">SQL</param>
		/// <param name="parameters">Параметры</param>
		/// <returns>Список из базы</returns>
		public async Task<List<object[]>> Select(string sql, Dictionary<string, object> parameters = null)
		{
			var command = new SqlCommand(sql, _connection);
			if (parameters != null)
			{
				foreach (var param in parameters)
				{
					command.Parameters.AddWithValue(param.Key, param.Value);
				}
			}

			try
			{
				_connection.Open();
				using var reader = await command.ExecuteReaderAsync();
				var list = new List<object[]>();
				while (await reader.ReadAsync())
				{
					var objects = new object[reader.FieldCount];
					reader.GetValues(objects);
					list.Add(objects);
				}
				return list;
			}
			finally
			{
				_connection.Close();
			}
		}

		/// <summary>
		/// Уничтожает подключение
		/// </summary>
		public void Dispose()
		{
			_connection?.Dispose();
		}
	}
}
