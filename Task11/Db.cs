﻿using LinqToDB;
using LinqToDB.Data;
using Norbit.Crm.Krikunov.Task11.DbEntitys;
using System.Configuration;

namespace Norbit.Crm.Krikunov.Task11
{
	/// <summary>
	/// Подключение Linq2Db
	/// </summary>
	public class Db : DataConnection
	{
		/// <summary>
		/// Предметы.
		/// </summary>
		public ITable<Subject> Subjects => GetTable<Subject>();

		/// <summary>
		/// Преподователи.
		/// </summary>
		public ITable<Teacher> Teachers => GetTable<Teacher>();

		/// <summary>
		/// Связь предметов и преподователей.
		/// </summary>
		public ITable<SubjectTeacherMap> SubjectTeachers => GetTable<SubjectTeacherMap>();

		/// <summary>
		/// Расписание..
		/// </summary>
		public ITable<Schedule> Schedules => GetTable<Schedule>();

		/// <summary>
		/// Создание подключения.
		/// </summary>
		/// <param name="connection">Строка подключения</param>
		private Db(string connection) : base(ProviderName.SqlServer2017, connection) { }

		/// <summary>
		/// Создание экземпляра.
		/// </summary>
		/// <returns>Экземпляра</returns>
		public static Db NewInstance()
		{
			var connectionString = ConfigurationManager.AppSettings.Get("connection");
			return new Db(connectionString);
		}
	}
}
