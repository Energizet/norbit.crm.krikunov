﻿using Norbit.Crm.Krikunov.Libs;
using System;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;

namespace Norbit.Crm.Krikunov.Task11
{
	class Program
	{
		static async Task Main()
		{
			var commands = new CommandBuilder()
				.Add("Показать расписание", ShowSchedule)
				.Add("Вывести связку учителей и предметов", ShowSubjectTeacher)
				.Add("Установить время предмета", SetTimestamp)
				.Build();
			await ConsoleGui.Loop(commands);
		}

		/// <summary>
		/// Вывести расписание
		/// </summary>
		static async Task ShowSchedule()
		{
			using var sql = new Sql();
			var schedule = await sql.Select(@"SELECT
G.[Name] AS [Group],
S.[Name] AS [Subject],
T.[Name] AS [Teacher],
SC.[Timestamp] AS [Time]
FROM Schedule AS SC
INNER JOIN SubjectTeacherMap AS STM ON SC.SubjectTeacherId = STM.Id
INNER JOIN [Subject] AS S ON STM.SubjectId = S.Id
INNER JOIN Teacher AS T ON STM.TeacherId = T.Id
INNER JOIN [Group] AS G ON SC.GroupId = G.Id
ORDER BY SC.[Timestamp]");
			foreach (var arr in schedule)
			{
				Console.WriteLine($"{arr[0],-10} {arr[1],-20} {arr[2],-40} {arr[3],-20}");
			}
		}

		/// <summary>
		/// Вывести связку учителей и предметов
		/// </summary>
		static void ShowSubjectTeacher()
		{
			using var db = Db.NewInstance();
			var subjectsTeachers = from stm in db.SubjectTeachers
								   from s in db.Subjects.FullJoin(s => s.Id == stm.SubjectId)
								   from t in db.Teachers.FullJoin(t => t.Id == stm.TeacherId)
								   select new { stm.Id, Subject = s.Name, Teacher = t.Name };
			foreach (var item in subjectsTeachers)
			{
				Console.WriteLine($"{item.Id,-40} {item.Subject,-20} {item.Teacher,-20}");
			}
		}

		/// <summary>
		/// Установить время предмета
		/// </summary>
		static void SetTimestamp()
		{
			Console.Write("Введите время(число): ");
			if (!int.TryParse(Console.ReadLine(), out var timestamp))
			{
				Console.WriteLine("Не верное число");
				return;
			}

			using var db = Db.NewInstance();
			db.Schedules
				.Where(s => s.Id == new Guid("476263AE-7BDF-4E56-9ECB-AB11E964D6E2"))
				.Set(s => s.Timestamp, timestamp)
				.Update();
			Console.WriteLine("Время установленно");
		}
	}
}
