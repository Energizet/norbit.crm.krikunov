﻿using LinqToDB.Mapping;
using System;

namespace Norbit.Crm.Krikunov.Task11.DbEntitys
{
	/// <summary>
	/// Предмет-Учитель
	/// </summary>
	[Table]
	public class SubjectTeacherMap
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Id предмета.
		/// </summary>
		[Column, NotNull]
		public Guid SubjectId { get; set; }

		/// <summary>
		/// Id учителя.
		/// </summary>
		[Column, NotNull]
		public Guid TeacherId { get; set; }
	}
}
