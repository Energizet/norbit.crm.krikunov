﻿using LinqToDB.Mapping;
using System;

namespace Norbit.Crm.Krikunov.Task11.DbEntitys
{
	/// <summary>
	/// Предмет
	/// </summary>
	[Table]
	public class Subject
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Название.
		/// </summary>
		[Column, NotNull]
		public string Name { get; set; }
	}
}
