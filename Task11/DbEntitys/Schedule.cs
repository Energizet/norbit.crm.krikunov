﻿using LinqToDB.Mapping;
using System;

namespace Norbit.Crm.Krikunov.Task11.DbEntitys
{
	/// <summary>
	/// Расписание.
	/// </summary>
	[Table]
	public class Schedule
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Id группы
		/// </summary>
		[Column, NotNull]
		public Guid GroupId { get; set; }

		/// <summary>
		/// Id учитель-предмет.
		/// </summary>
		[Column, NotNull]
		public Guid SubjectTeacherId { get; set; }

		/// <summary>
		/// Время
		/// </summary>
		[Column, NotNull]
		public int Timestamp { get; set; }

	}
}
