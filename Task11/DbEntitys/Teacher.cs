﻿using LinqToDB.Mapping;
using System;

namespace Norbit.Crm.Krikunov.Task11.DbEntitys
{
	/// <summary>
	/// Учитель.
	/// </summary>
	[Table]
	public class Teacher
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// ФИО
		/// </summary>
		[Column, NotNull]
		public string Name { get; set; }
	}
}
