﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Libs;

namespace Norbit.Crm.Krikunov.Task9
{
	class Program
	{
		static async Task Main()
		{
			var commands = new CommandBuilder()
				.Add("Показать структуру папки", ShowDirs)
				.Add("Прочитать файл TextFile.txt", ReadFile)
				.Add("Дописать в файл TextFile.txt", WriteFile)
				.Add("Побайтовое копировать файла TextFile.txt", BytesCopyFile)
				.Build();
			await ConsoleGui.Loop(commands);
		}

		/// <summary>
		/// Вывод папок в корне диска.
		/// </summary>
		static void ShowDirs()
		{
			var directories = Directory.EnumerateDirectories("/");
			foreach (var directory in directories)
			{
				Console.WriteLine(directory);
			}
		}

		/// <summary>
		/// Чтение файла TextFile.txt.
		/// </summary>
		static async Task ReadFile()
		{
			var file = await File.ReadAllTextAsync("./TextFile.txt");
			Console.WriteLine(file);
		}

		/// <summary>
		/// Запись в файл TextFile.txt.
		/// </summary>
		static async Task WriteFile()
		{
			Console.WriteLine("Введите строку:");
			var line = Console.ReadLine();
			var writer = new StreamWriter("./TextFile.txt", true);
			await writer.WriteLineAsync(line);
			writer.Close();
		}

		/// <summary>
		/// Побайтовое копировать файла TextFile.txt.
		/// </summary>
		static async Task BytesCopyFile()
		{
			var blockingCopy = new BlockingCopy("./TextFile.txt", "./TextFile1.txt", 1);
			blockingCopy.StartCopyEventHandler += (sender, e) => Console.WriteLine("Копирование началось");
			blockingCopy.EndCopyEventHandler += (sender, e) => Console.WriteLine("Копирование закончилось");
			blockingCopy.ProgressCopyEventHandler +=
				(sender, progress) => Console.WriteLine($"Прогресс: {Math.Round(progress * 100)}%");
			await blockingCopy.CopyAsync();
		}
	}
}
