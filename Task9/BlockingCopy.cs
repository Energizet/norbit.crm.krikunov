﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task9
{
	/// <summary>
	///  Блочное побайтовой копирование из одного файла в другой.
	/// </summary>
	public class BlockingCopy
	{
		/// <summary>
		/// Путь к исходному файлу.
		/// </summary>
		private readonly string _from;
		/// <summary>
		/// Путь к конечному файлу
		/// </summary>
		private readonly string _to;
		/// <summary>
		/// Размер исходного файла.
		/// </summary>
		private readonly long _fileSize;
		/// <summary>
		/// Размер копируемого блока.
		/// </summary>
		private readonly int _blockSize;
		/// <summary>
		/// Поток чтения.
		/// </summary>
		private FileStream _fromStream;
		/// <summary>
		/// Поток записи.
		/// </summary>
		private FileStream _toStream;

		/// <summary>
		/// Последнее исключение.
		/// </summary>
		public Exception LastException { get; private set; }

		/// <summary>
		/// Начало копирования.
		/// </summary>
		public event EventHandler StartCopyEventHandler;

		/// <summary>
		/// Прогресс копирования.
		/// </summary>
		public event EventHandler<double> ProgressCopyEventHandler;

		/// <summary>
		/// Завершение копирования.
		/// </summary>
		public event EventHandler EndCopyEventHandler;

		/// <summary>
		/// Слушатель исключений.
		/// </summary>
		public event EventHandler<Exception> ExceptionEventHandler;

		/// <summary>
		/// Устанавливает файл откуда читать, куда записывать и количество байт в блоке.
		/// </summary>
		/// <param name="from">Файл чтения</param>
		/// <param name="to">Файл записи</param>
		/// <param name="blockSize">Размер блока</param>
		public BlockingCopy(string from, string to, int blockSize)
		{
			if (!File.Exists(from))
			{
				throw new FileNotFoundException("Файл для чтения не найден");
			}

			if (blockSize <= 0)
			{
				throw new ArgumentOutOfRangeException($"Размер блока должент быть положительным");
			}

			_from = from;
			_to = to;
			_fileSize = new FileInfo(_from).Length;
			_blockSize = blockSize;
		}

		/// <summary>
		/// Запуск копирования.
		/// </summary>
		/// <returns></returns>
		public async Task<bool> CopyAsync()
		{
			try
			{
				OpenReadStream();
				OpenWriteStream();

				StartCopyEventHandler?.Invoke(this, new EventArgs());
				await Coping();
				EndCopyEventHandler?.Invoke(this, new EventArgs());
				return true;
			}
			catch (Exception ex)
			{
				LastException = ex;
				ExceptionEventHandler?.Invoke(this, LastException);
				return false;
			}
			finally
			{
				_fromStream?.Close();
				_toStream?.Close();
			}
		}

		/// <summary>
		/// Создание потока чтения.
		/// </summary>
		private void OpenReadStream()
		{
			try
			{
				_fromStream = new FileStream(_from, FileMode.Open, FileAccess.Read);
			}
			catch (Exception ex)
			{
				throw new FileLoadException("Не удалось открыть файл для чтения", ex);
			}
		}

		/// <summary>
		/// Создание потока записи.
		/// </summary>
		private void OpenWriteStream()
		{
			try
			{
				_toStream = new FileStream(_to, FileMode.Create, FileAccess.Write);
			}
			catch (Exception ex)
			{
				throw new FileLoadException("Не удалось открыть файл для записи", ex);
			}
		}

		/// <summary>
		/// Копирование
		/// </summary>
		private async Task Coping()
		{
			var progress = 0;
			var buffer = new byte[_blockSize];
			while (progress < _fileSize)
			{
				var read = await _fromStream.ReadAsync(buffer, 0, buffer.Length);
				await _toStream.WriteAsync(buffer, 0, read);
				progress += read;
				ProgressCopyEventHandler?.Invoke(this, 1f * progress / _fileSize);
			}
		}
	}
}