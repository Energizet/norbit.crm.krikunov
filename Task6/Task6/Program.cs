﻿using Norbit.Crm.Krikunov.Libs;

namespace Norbit.Crm.Krikunov.Task6
{
	class Program
	{
		static void Main()
		{
			var io = new ConsoleIo();
			var task = new Task6(io, io);
			task.Run();
		}
	}
}
