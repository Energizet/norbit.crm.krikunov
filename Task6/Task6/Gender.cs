﻿namespace Norbit.Crm.Krikunov.Task6
{
	/// <summary>
	/// Пол
	/// </summary>
	public enum Gender
	{
		Male = 0,
		Female = 1
	}
}