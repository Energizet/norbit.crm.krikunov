﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Norbit.Crm.Krikunov.Libs;

namespace Norbit.Crm.Krikunov.Task6
{
	/// <summary>
	/// В тестовой программе реализовать доступ к коллекции через LINQ с использованием:
	/// Фильтрации
	/// Анонимных типов
	/// группировки
	/// в одном из режимов реализовать запрос через FROM, а в других через лямбды
	/// научиться делать ToArray, ToList, Take, Skip, OrderBy, Any, First, FirstOrDefault и т.п.
	/// </summary>
	public class Task6 : IncludeIo
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="reader">Ввод</param>
		/// <param name="writer">Вывод</param>
		public Task6([NotNull] IReader reader, [NotNull] IWriter writer)
		{
			Reader = reader;
			Writer = writer;
		}

		/// <summary>
		/// Запуск задачи
		/// </summary>
		public void Run()
		{
			var peoples = new List<object>
			{
				new People("Антон",23,Gender.Male),
				new People("Иван",34,Gender.Male),
				new People("Ира",35,Gender.Female),
				new People("Никита",26,Gender.Male),
				new People("Юля",21,Gender.Female),
				25,
				null,
				"asd"
			};

			Writer.WriteLine("Старше 25");
			var older25 = from item in peoples
						  where item is People people && people.Age > 25
						  select item.ToString();
			older25.ToList().ForEach(item => Writer.WriteLine(item));

			Writer.WriteLine();
			Writer.WriteLine("Женщины");
			peoples.OfType<People>()
				.Where(people => people.Gender == Gender.Female)
				.ToList()
				.ForEach(item => Writer.WriteLine(item.ToString()));

			Writer.WriteLine();
			Writer.WriteLine("Первые два");
			peoples.Take(2).ToList().ForEach(item => Writer.WriteLine(item?.ToString() ?? "null"));

			Writer.WriteLine();
			Writer.WriteLine("Остальные");
			peoples.Skip(2).ToList().ForEach(item => Writer.WriteLine(item?.ToString() ?? "null"));

			Writer.WriteLine();
			Writer.WriteLine("Групировка по полу");
			peoples.OfType<People>()
				.GroupBy(people => people.Gender)
				.ToList()
				.ForEach(group =>
				{
					group.OrderBy(item=>item.Age)
						.ToList()
						.ForEach(item => Writer.WriteLine(item.ToString()));
					Writer.WriteLine();
				});
		}
	}
}