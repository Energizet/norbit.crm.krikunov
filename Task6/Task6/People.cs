﻿using JetBrains.Annotations;

namespace Norbit.Crm.Krikunov.Task6
{
	/// <summary>
	/// Человек
	/// </summary>
	public class People
	{
		/// <summary>
		/// Имя
		/// </summary>
		public string Name;

		/// <summary>
		/// Возраст
		/// </summary>
		public int Age;

		/// <summary>
		/// Пол
		/// </summary>
		public Gender Gender;

		/// <summary>
		/// Конструктор человека
		/// </summary>
		/// <param name="name">Имя</param>
		/// <param name="age">Возраст</param>
		/// <param name="gender">Пол</param>
		public People([NotNull] string name, int age, Gender gender)
		{
			Name = name;
			Age = age;
			Gender = gender;
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"{Name} {Age} - {Gender}";
		}
	}
}