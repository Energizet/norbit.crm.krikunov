﻿namespace Norbit.Crm.Krikunov.Libs
{
	/// <summary>
	/// Чтения данных
	/// </summary>
	public interface IReader
	{
		/// <summary>
		/// Читать строку
		/// </summary>
		/// <returns>Прочитаная строка</returns>
		public string ReadLine();
	}
}