﻿using System;
using JetBrains.Annotations;

namespace Norbit.Crm.Krikunov.Libs
{
	/// <summary>
	/// Консольный ввод/вывод
	/// </summary>
	public class ConsoleIo : IReader, IWriter
	{
		/// <inheritdoc/>
		public string ReadLine()
		{
			return Console.ReadLine();
		}

		/// <inheritdoc/>
		public bool WriteLine([NotNull] string text = "")
		{
			Console.WriteLine(text);
			return true;
		}
	}
}