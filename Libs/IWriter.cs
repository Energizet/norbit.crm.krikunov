﻿using JetBrains.Annotations;

namespace Norbit.Crm.Krikunov.Libs
{
	/// <summary>
	/// Запись данных
	/// </summary>
	public interface IWriter
	{
		/// <summary>
		/// Записать строку
		/// </summary>
		/// <param name="text">Строка</param>
		/// <returns>true - если запись удачна</returns>
		public bool WriteLine([NotNull] string text = "");
	}
}