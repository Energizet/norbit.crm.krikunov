﻿namespace Norbit.Crm.Krikunov.Libs
{
	/// <summary>
	/// Включает ввод/вывод
	/// </summary>
	public abstract class IncludeIo
	{
		/// <summary>
		/// Ввод
		/// </summary>
		protected IReader Reader;

		/// <summary>
		/// Вывод
		/// </summary>
		protected IWriter Writer;
	}
}