﻿using System;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Libs
{
	/// <summary>
	/// Команды GUI
	/// </summary>
	public class Command
	{
		/// <summary>
		/// Отображаемый текст
		/// </summary>
		public readonly string Title;
		/// <summary>
		/// Вывоз после выбора
		/// </summary>
		public readonly Func<Task> Callback;

		/// <summary>
		/// Создание команды
		/// </summary>
		/// <param name="title">Текст</param>
		/// <param name="callback">Callback</param>
		public Command(string title, Action callback) : this(title, () => Task.Run(callback))
		{
		}

		/// <summary>
		/// Создание async команды
		/// </summary>
		/// <param name="title">Текст</param>
		/// <param name="callback">Callback</param>
		public Command(string title, Func<Task> callback)
		{
			Title = title;
			if (callback != null)
			{
				Callback = callback;
			}
		}

	}
}