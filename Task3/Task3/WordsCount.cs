﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Norbit.Crm.Krikunov.Task3
{
	/// <summary>
	/// Подсчёт слов частоты слов
	/// </summary>
	public static class WordsCount
	{
		/// <summary>
		/// Запуск задачи
		/// </summary>
		public static void Run(int topWords = 5)
		{
			Dictionary<string, int> words = new Dictionary<string, int>();

			Console.WriteLine("Введите текст");
			var str = Console.ReadLine();
			var regex = new Regex("[\\w-]+", RegexOptions.IgnoreCase);
			var matches = regex.Matches(str ?? "");

			foreach (Match match in matches)
			{
				var word = match.Value.Trim().ToLower();
				if (!words.ContainsKey(word))
				{
					words.Add(word, 0);
				}

				words[word]++;
			}

			var sortedWords = words.ToList();
			sortedWords.Sort((l, r) => l.Value.CompareTo(r.Value) * -1);

			Console.WriteLine();
			for (int i = 0; i < sortedWords.Count && i < topWords; i++)
			{
				Console.WriteLine($"{sortedWords[i].Key} - {sortedWords[i].Value}");
			}
		}
	}
}