﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Norbit.Crm.Krikunov.Task3
{
	/// <summary>
	/// Динамический массив
	/// </summary>
	public class DynamicArray<T> : IEnumerable<T>, ICloneable, IComparable
	{
		/// <summary>
		/// Массив значений
		/// </summary>
		private T[] _array;
		/// <summary>
		/// Размер заполненого массива
		/// </summary>
		private int _count;

		/// <summary>
		/// Длина массива
		/// </summary>
		public int Length => _count;

		/// <summary>
		/// Количество слотов в массиве
		/// </summary>
		public int Capacity => _array.Length;

		/// <summary>
		/// Установка и получения элемента в указаной позиции
		/// </summary>
		/// <param name="i">Позиция</param>
		public T this[int i]
		{
			get => Get(i);
			set => Set(i, value);
		}

		/// <summary>
		/// Инициализация динамического массива
		/// </summary>
		/// <param name="count">Начальный размер массива, по-умолчанию 8</param>
		public DynamicArray(int count = 8)
		{
			_count = 0;
			_array = new T[count];
		}

		/// <summary>
		/// Инициализирует динамический массив копией данных из IEnumerable
		/// </summary>
		public DynamicArray(IEnumerable<T> enumerable)
		{
			_count = 0;
			AddRange(enumerable);
		}

		/// <summary>
		/// Добавляет элемент в конец массива
		/// </summary>
		/// <param name="item">Элемент</param>
		/// <returns>true - если удалось добавить элемент</returns>
		public bool Add(T item)
		{
			try
			{
				if (_count >= _array.Length)
				{
					_array = Copy(new T[_array.Length * 2], _array);
				}

				_array[_count++] = item;
				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Добавляет несколько элементов
		/// </summary>
		/// <param name="enumerable">Элементы</param>
		/// <returns>true - если удалось добавить элемент</returns>
		public bool AddRange(IEnumerable<T> enumerable)
		{
			try
			{
				using var enumerator = enumerable.GetEnumerator();

				var enumerableCount = 0;
				enumerator.Reset();
				while (enumerator.MoveNext())
				{
					enumerableCount++;
				}

				if (_count + enumerableCount >= (_array?.Length ?? 0))
				{
					_array = Copy(new T[(_count + enumerableCount) * 2], _array);
				}

				enumerator.Reset();
				while (enumerator.MoveNext())
				{
					_array![_count++] = enumerator.Current;
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Вставляет элемент в указаную позицию
		/// </summary>
		/// <param name="position">Позиция в которую вставляется элемент</param>
		/// <param name="item">Элемент</param>
		/// <returns>true - если удалось добавить элемент</returns>
		public bool Insert(int position, T item)
		{
			if (position < 0 || position > _array.Length)
			{
				throw new ArgumentOutOfRangeException($"Попытка добавить элемент вне границ массива");
			}
			try
			{
				var tmpArray = new T[_array.Length * (_count >= _array.Length ? 2 : 1)];
				var tmpI = 0;
				for (var i = 0; i < _count; i++, tmpI++)
				{
					if (i == position)
					{
						tmpArray[i] = item;
						tmpI++;
					}

					tmpArray[tmpI] = _array[i];
				}

				_count = tmpI;
				_array = tmpArray;

				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Удаляет элемент из массива
		/// </summary>
		/// <param name="item">Элемент</param>
		/// <returns>true - если элемент удалён</returns>
		public bool Remove(T item)
		{
			try
			{
				var index = IndexOf(item);
				if (index < 0)
				{
					return false;
				}

				var tmpArray = new T[_array.Length];
				var tmpI = 0;
				for (var i = 0; i < _count; i++, tmpI++)
				{
					if (i == index)
					{
						i++;
					}

					tmpArray[tmpI] = _array[i];
				}

				_count = tmpI;
				_array = tmpArray;

				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Получает элемент по заданой позиции
		/// </summary>
		/// <param name="position">Заданая позиция в пределах массива, иначе ArgumentOutOfRangeException</param>
		/// <returns>Элемент если позиция внутри массива</returns>
		public T Get(int position)
		{
			if (position < 0 || position > _array.Length)
			{
				throw new ArgumentOutOfRangeException($"Попытка получить элемент вне границ массива");
			}

			try
			{
				return _array[position];
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				throw;
			}
		}

		/// <summary>
		/// Устанавливает элемент в указаную позицию
		/// </summary>
		/// <param name="position">Заданая позиция в пределах массива, иначе ArgumentOutOfRangeException</param>
		/// <param name="item">Элемент</param>
		public void Set(int position, T item)
		{
			if (position < 0 || position > _array.Length)
			{
				throw new ArgumentOutOfRangeException($"Попытка установить элемент вне границ массива");
			}

			try
			{
				_array[position] = item;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				throw;
			}
		}

		/// <summary>
		/// Возвращает индекс искомого элемента
		/// </summary>
		/// <param name="item">Искомый элемент</param>
		/// <returns>-1 - если не элемент не найден, иначе положительное число</returns>
		public int IndexOf(T item)
		{
			for (var i = 0; i < _count; i++)
			{
				if (_array[i].Equals(item))
				{
					return i;
				}
			}

			return -1;
		}

		/// <summary>
		/// Копирует элементы из одного массива в другой
		/// </summary>
		/// <param name="to">Куда копировать</param>
		/// <param name="from">Откуда копировать</param>
		/// <returns>Скопированый массив</returns>
		private T[] Copy(T[] to, T[] from)
		{
			for (var i = 0; i < (from?.Length ?? 0) && i < to.Length; i++)
			{
				to[i] = from![i];
			}

			return to;
		}

		/// <inheritdoc />
		public IEnumerator GetEnumerator()
		{
			return new ItemEnumerator(_array, _count);
		}


		/// <inheritdoc />
		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return (IEnumerator<T>)GetEnumerator();
		}

		/// <summary>
		/// Клонирует массив
		/// </summary>
		/// <returns>Клонированый массив</returns>
		public object Clone()
		{
			return new DynamicArray<T>(_array);
		}

		/// <summary>
		/// Сравнение массивов
		/// </summary>
		/// <param name="obj">Другой массив</param>
		/// <returns>Стандартные правила CompareTo</returns>
		public int CompareTo(object obj)
		{
			if (obj == null)
			{
				return -1;
			}

			var array = obj as DynamicArray<T>;
			if (array == null)
			{
				return -1;
			}

			return _count.CompareTo(array._count);
		}

		/// <summary>
		/// Приватный IEnumerator для массива
		/// </summary>
		private class ItemEnumerator : IEnumerator<T>
		{
			/// <summary>
			/// Итерируемый массив
			/// </summary>
			private readonly T[] _array;
			/// <summary>
			/// Количество элементов в массиве
			/// </summary>
			private readonly int _count;
			/// <summary>
			/// Итерируемая позиция
			/// </summary>
			private int _position = -1;

			/// <summary>
			/// Возвращает текущий элемент или null
			/// </summary>
			public object Current
			{
				get
				{
					if (_position >= 0 && _position < _array.Length)
					{
						return _array[_position];
					}

					return null;
				}
			}

			/// <summary>
			/// Возвращает элемент приведённый к нужному типу
			/// </summary>
			T IEnumerator<T>.Current => (T)Current;

			/// <summary>
			/// Инициализирует енумератор массивом и размером массива
			/// </summary>
			/// <param name="array">Массив</param>
			/// <param name="count">Размер массива</param>
			public ItemEnumerator(T[] array, int count)
			{
				_array = array;
				_count = count;
			}
			
			/// <inheritdoc />
			public bool MoveNext()
			{
				_position++;
				return _position < _count && _position < _array.Length;
			}

			/// <inheritdoc />
			public void Reset()
			{
				_position = -1;
			}

			/// <inheritdoc />
			public void Dispose()
			{
				Reset();
				GC.SuppressFinalize(this);
			}

			/// <summary>
			/// Вызывает Dispose во время уничтожения
			/// </summary>
			~ItemEnumerator()
			{
				Dispose();
			}
		}
	}
}