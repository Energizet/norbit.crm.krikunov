﻿using System;
using System.Collections.Generic;

namespace Norbit.Crm.Krikunov.Task3
{
	/// <summary>
	/// Пример работы с динамическим массивом
	/// </summary>
	public static class DynamicArrayExample
	{
		/// <summary>
		/// Запуск задачи
		/// </summary>
		public static void Run()
		{
			var dynamic = new DynamicArray<int>(2);

			dynamic.Add(1);
			Console.WriteLine($"Add 1: {dynamic.Length} - {dynamic.Capacity}");

			dynamic.Add(21);
			Console.WriteLine($"Add 21: {dynamic.Length} - {dynamic.Capacity}");

			dynamic.Add(145);
			Console.WriteLine($"Add 145: {dynamic.Length} - {dynamic.Capacity}");

			dynamic.AddRange(new[] { 5, 6 });
			Console.WriteLine($"Add 5, 6: {dynamic.Length} - {dynamic.Capacity}");

			Console.WriteLine(string.Join(", ", dynamic));
			dynamic.Remove(21);
			Console.WriteLine($"Remove 21: {string.Join(", ", dynamic)}");

			dynamic.Insert(1, 22);
			Console.WriteLine($"Insert to 1 - 22: {string.Join(", ", dynamic)}");

			dynamic.Insert(1, 23);
			Console.WriteLine($"Insert to 1 - 23: {string.Join(", ", dynamic)}");

			dynamic[1] = 25;
			Console.WriteLine($"Set to 1 - 25: {string.Join(", ", dynamic)}");

			Console.WriteLine(string.Join(", ", dynamic));
			Console.WriteLine($"Количество элементов в массиве - ёмкость массива: {dynamic.Length} - {dynamic.Capacity}");

			var list = new List<int> { 5, 6, 7 };
			dynamic = new DynamicArray<int>(list);
			dynamic.Add(8);
		}
	}
}