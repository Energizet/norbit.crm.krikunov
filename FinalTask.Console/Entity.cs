﻿using System;

namespace Norbit.Crm.Krikunov.FinalTask.InConsole
{
	/// <summary>
	/// Сделка
	/// </summary>
	public class Entity
	{
		//string не баг, а фича
		//если сделать double - возвращает 7.722921193729351e+23
		/// <summary>
		/// Число?!
		/// </summary>
		public string Amount { get; set; }

		/// <summary>
		/// Дата.
		/// </summary>
		public DateTime Created { get; set; }
	}
}