﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.FinalTask.InConsole
{
	class Program
	{
		static async Task Main()
		{
			var client = new HttpClient(new HttpClientHandler
			{
				UseDefaultCredentials = true
			});
			var json = await client.GetStringAsync(@"https://localhost:44393/api/entity");
			var entity = JsonSerializer.Deserialize<Entity>(json);
			Console.WriteLine($"Created = {entity.Created.ToLocalTime()}\nAmount = {entity.Amount}");
		}
	}
}
