﻿using System;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;
using Norbit.Crm.Krikunov.Task7.UseCases;

namespace Norbit.Crm.Krikunov.Task7.Controllers.RoundController
{
	/// <summary>
	/// Реализания конторллера круга
	/// </summary>
	public class RoundController : AbstractRoundController
	{
		/// <inheritdoc />
		public override void PrintRound(Round round)
		{
			Console.WriteLine($"Радиус = {round.Radius}");
			Console.WriteLine($"Центр = {round.Center.X};{round.Center.Y}");
			Console.WriteLine($"Окружность = {round.Circle}");
			Console.WriteLine($"Площадь = {round.S}");
		}

		/// <summary>
		/// Запуск выполнения задачи по кругу
		/// </summary>
		public static async Task Run()
		{
			var roundCase = new RoundUseCase(new RoundController(), new RoundRepository());
			await roundCase.GetAndPrintRound();
		}
	}
}