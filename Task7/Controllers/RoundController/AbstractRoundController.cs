﻿using System;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task7.Controllers.RoundController
{
	/// <summary>
	/// Реализания конторллера круга
	/// </summary>
	public abstract class AbstractRoundController : IRoundController
	{

		/// <inheritdoc/>
		public abstract void PrintRound(Round round);

		/// <inheritdoc/>
		public virtual void PrintException(Exception ex, bool showStack = true)
		{
			Console.WriteLine(showStack ? ex.ToString() : ex.Message);
		}
	}
}