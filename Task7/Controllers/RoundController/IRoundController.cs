﻿using System;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task7.Controllers.RoundController
{
	/// <summary>
	/// Контроллер круга
	/// </summary>
	public interface IRoundController
	{
		/// <summary>
		/// Печатать круг
		/// </summary>
		/// <param name="round">Круг</param>
		public void PrintRound(Round round);

		/// <summary>
		/// Вывод исключения.
		/// </summary>
		/// <param name="ex">Исключение</param>
		/// <param name="showStack">true - полная информация, false - только message</param>
		public void PrintException(Exception ex, bool showStack = true);
	}
}