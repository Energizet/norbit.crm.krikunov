﻿using Norbit.Crm.Krikunov.Task7.Entities;
using System;

namespace Norbit.Crm.Krikunov.Task7.Controllers.EmployeeController
{
	public abstract class AbstractUserController : IEmployeeController
	{
		/// <inheritdoc/>
		public abstract void PrintEmployee(Employee employee);

		/// <inheritdoc/>
		public virtual void PrintException(Exception ex, bool showStack = true)
		{
			Console.WriteLine(showStack ? ex.ToString() : ex.Message);
		}
	}
}
