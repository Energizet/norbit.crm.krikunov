﻿using System;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task7.Controllers.EmployeeController
{
	/// <summary>
	/// Контроллер сотрудника
	/// </summary>
	public interface IEmployeeController
	{
		/// <summary>
		/// Печатать сотрудника
		/// </summary>
		/// <param name="employee">Сотрудник</param>
		public void PrintEmployee(Employee employee);

		/// <summary>
		/// Вывод исключения.
		/// </summary>
		/// <param name="ex">Исключение</param>
		/// <param name="showStack">true - полная информация, false - только message</param>
		public void PrintException(Exception ex, bool showStack = true);
	}
}