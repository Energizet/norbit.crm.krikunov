﻿using System;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;
using Norbit.Crm.Krikunov.Task7.UseCases;

namespace Norbit.Crm.Krikunov.Task7.Controllers.EmployeeController
{
	/// <summary>
	/// Реализания конторллера сотрудника
	/// </summary>
	public class EmployeeController : AbstractUserController
	{
		/// <inheritdoc />
		public override void PrintEmployee(Employee employee)
		{
			Console.WriteLine(employee.ToString());
		}

		/// <summary>
		/// Запуск выполнения задачи по сотрудникам
		/// </summary>
		public static async Task Run()
		{
			var useCase = new EmployeeUseCase(new EmployeeController(), new EmployeeRepository());
			await useCase.GetAndPrintEmployee();
		}
	}
}