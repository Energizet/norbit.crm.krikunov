﻿using System;
using Norbit.Crm.Krikunov.Task7.Entities;

namespace Norbit.Crm.Krikunov.Task7.Controllers.UserController
{
	/// <summary>
	/// Контроллер пользователя
	/// </summary>
	public interface IUserController
	{
		/// <summary>
		/// Печатать пользователя
		/// </summary>
		/// <param name="user">Пользователь</param>
		public void PrintUser(User user);

		/// <summary>
		/// Вывод исключения.
		/// </summary>
		/// <param name="ex">Исключение</param>
		/// <param name="showStack">true - полная информация, false - только message</param>
		public void PrintException(Exception ex, bool showStack = true);
	}
}