﻿using Norbit.Crm.Krikunov.Task7.Entities;
using System;

namespace Norbit.Crm.Krikunov.Task7.Controllers.UserController
{
	public abstract class AbstractUserController : IUserController
	{
		/// <inheritdoc/>
		public abstract void PrintUser(User user);

		/// <inheritdoc/>
		public virtual void PrintException(Exception ex, bool showStack = true)
		{
			Console.WriteLine(showStack ? ex.ToString() : ex.Message);
		}
	}
}
