﻿using System;
using System.Drawing;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task7.Repositories
{
	/// <inheritdoc cref="IRoundRepository"/>/>
	public class RoundRepository : IRoundRepository
	{
		/// <inheritdoc />
		public async Task<double> GetRadius()
		{
			return await Task.Run(() =>
			{
				while (true)
				{
					Console.Write("Введите радиус: ");
					var radiusStr = Console.ReadLine();
					if (int.TryParse(radiusStr, out var radius))
					{
						return radius;
					}

					Console.WriteLine("Неверное число");
				}
			});
		}

		public async Task<Point> GetCenter()
		{
			return await Task.Run(() =>
			{
				while (true)
				{
					Console.Write("Введите X центра: ");
					var xStr = Console.ReadLine();
					if (int.TryParse(xStr, out var x))
					{
						Console.Write("Введите Y центра: ");
						var yStr = Console.ReadLine();
						if (int.TryParse(yStr, out var y))
						{
							return new Point(x, y);
						}
					}

					Console.WriteLine("Неверное число");
				}
			});
		}
	}
}