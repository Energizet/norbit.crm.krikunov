﻿using System.Drawing;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task7.Repositories
{
	/// <summary>
	/// Получение данных круга
	/// </summary>
	public interface IRoundRepository
	{
		/// <summary>
		/// Получить радиус
		/// </summary>
		/// <returns>Радиус</returns>
		public Task<double> GetRadius();

		/// <summary>
		/// Получить центр круга
		/// </summary>
		/// <returns>Центр круга</returns>
		public Task<Point> GetCenter();
	}
}