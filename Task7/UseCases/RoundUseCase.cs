﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Task7.Controllers.RoundController;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;

namespace Norbit.Crm.Krikunov.Task7.UseCases
{
	/// <summary>
	/// UseCases на круг
	/// </summary>
	public class RoundUseCase
	{
		/// <summary>
		/// Контроллер (Вывод)
		/// </summary>
		private readonly IRoundController _controller;
		/// <summary>
		/// Репозиторий (Ввод)
		/// </summary>
		private readonly IRoundRepository _repository;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="controller">Контроллер</param>
		/// <param name="repository">Репозиторий</param>
		public RoundUseCase(IRoundController controller, IRoundRepository repository)
		{
			_controller = controller;
			_repository = repository;
		}

		/// <summary>
		/// Запуск use-case 
		/// </summary>
		public async Task GetAndPrintRound()
		{
			var round = new Round(1, new Point());

			var tryCount = 0;
			while (tryCount < 3)
			{
				try
				{
					tryCount++;
					round.Radius = await _repository.GetRadius();
					break;
				}
				catch (Exception ex)
				{
					_controller.PrintException(ex, false);
				}
			}

			if (tryCount >= 3)
			{
				_controller.PrintException(new ArgumentException($"Вы ошиблись 3 раза!"), false);
				return;
			}

			tryCount = 0;
			while (tryCount < 3)
			{
				try
				{
					tryCount++;
					round.Center = await _repository.GetCenter();
					break;
				}
				catch (Exception ex)
				{
					_controller.PrintException(ex, false);
				}
			}

			if (tryCount >= 3)
			{
				_controller.PrintException(new ArgumentException($"Вы ошиблись 3 раза!"), false);
				return;
			}
			_controller.PrintRound(round);
		}
	}
}