﻿using System;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Task7.Controllers.EmployeeController;
using Norbit.Crm.Krikunov.Task7.Entities;
using Norbit.Crm.Krikunov.Task7.Repositories;

namespace Norbit.Crm.Krikunov.Task7.UseCases
{
	/// <summary>
	/// UseCases на сотрудника
	/// </summary>
	public class EmployeeUseCase
	{
		/// <summary>
		/// Контроллер (Вывод)
		/// </summary>
		private readonly IEmployeeController _controller;
		/// <summary>
		/// Репозиторий (Ввод)
		/// </summary>
		private readonly IEmployeeRepository _repository;

		/// <summary>
		/// Конструктор (Спасибо, кеп)
		/// </summary>
		/// <param name="controller">Контроллер</param>
		/// <param name="repository">Репозиторий</param>
		public EmployeeUseCase(IEmployeeController controller, IEmployeeRepository repository)
		{
			_controller = controller;
			_repository = repository;
		}

		/// <summary>
		/// Запуск use-case 
		/// </summary>
		public async Task GetAndPrintEmployee()
		{
			var employee = new Employee
			{
				FirstName = await _repository.GetFirstName(),
				LastName = await _repository.GetLastName(),
				MiddleName = await _repository.GetMiddleName(),
			};

			var tryCount = 0;
			while (tryCount < 3)
			{
				try
				{
					tryCount++;
					var birthDay = await _repository.GetBirthDay();
					employee.BirthDay = birthDay;
					break;
				}
				catch (Exception ex)
				{
					_controller.PrintException(ex, false);
				}
			}

			if (tryCount >= 3)
			{
				_controller.PrintException(new ArgumentException($"Вы ошиблись 3 раза!"), false);
				return;
			}

			tryCount = 0;
			while (tryCount < 3)
			{
				try
				{
					tryCount++;
					var experience = await _repository.GetExperience();
					employee.Experience = experience;
					break;
				}
				catch (Exception ex)
				{
					_controller.PrintException(ex, false);
				}
			}

			if (tryCount >= 3)
			{
				_controller.PrintException(new ArgumentException($"Вы ошиблись 3 раза!"), false);
				return;
			}

			employee.Position = await _repository.GetPosition();

			_controller.PrintEmployee(employee);
		}
	}
}