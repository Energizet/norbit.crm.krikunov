﻿using System;
using System.Drawing;

namespace Norbit.Crm.Krikunov.Task7.Entities
{
	/// <summary>
	/// Круг
	/// </summary>
	public class Round
	{
		/// <summary>
		/// Радиус
		/// </summary>
		private double _radius;

		/// <summary>
		/// Радиус
		/// </summary>
		public double Radius
		{
			get => _radius;
			set
			{
				if (value <= 0)
				{
					throw new ArgumentException("Радиус должно быть больше 0");
				}
				_radius = value;
			}
		}

		/// <summary>
		/// Центр круга
		/// </summary>
		public Point Center { get; set; }

		/// <summary>
		/// Окружность
		/// </summary>
		public double Circle => 2 * Math.PI * _radius;

		/// <summary>
		/// Площадь
		/// </summary>
		public double S => Math.PI * Math.Pow(_radius, 2);

		/// <summary>
		/// Круг с указаным радиусом
		/// </summary>
		/// <param name="radius">Радиус</param>
		public Round(double radius, Point center)
		{
			Radius = radius;
			Center = center;
		}

	}
}