﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task5
{
	/// <summary>
	/// Сохраняет и считует JSON-файл
	/// </summary>
	public class JsonSerializer<T>
	{
		/// <summary>
		/// Путь к JSON-файлу
		/// </summary>
		private string Path => ConfigurationManager.AppSettings.Get("PathToJson");

		/// <summary>
		/// Читает и возвращает массив
		/// </summary>
		/// <returns>Массив из JSON-файла</returns>
		public async Task<IEnumerable<T>> Read()
		{
			try
			{
				var stream = File.OpenRead(Path);
				var json = await JsonSerializer.DeserializeAsync<IEnumerable<T>>(stream);
				await stream.DisposeAsync();
				return json;
			}
			catch (FileNotFoundException)
			{
				Console.WriteLine("Массив ещё не создан");
				return new T[0];
			}
		}

		/// <summary>
		/// Записывает массив в JSON-файл
		/// </summary>
		/// <param name="array">Массив</param>
		/// <returns>true - если запись удалась</returns>
		public async Task<bool> Write(IEnumerable<T> array)
		{
			try
			{
				var stream = File.Create(Path);
				await JsonSerializer.SerializeAsync(stream, array);
				await stream.DisposeAsync();
				return true;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				return false;
			}
		}
	}
}