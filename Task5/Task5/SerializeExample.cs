﻿using Norbit.Crm.Krikunov.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Norbit.Crm.Krikunov.Task5
{
	/// <summary>
	/// Пример использования GUI на примере чтения и записи JSON-файла
	/// </summary>
	public class SerializeExample
	{
		/// <summary>
		/// Запуск задачи
		/// </summary>
		public static async Task Run()
		{
			var example = new SerializeExample();
			var commands = new CommandBuilder()
				.Add("Прочитать список", example.ReadJson)
				.Add("Записать список", example.WriteJson)
				.Build();

			await ConsoleGui.Loop(commands);
		}

		/// <summary>
		/// Действие в меню Прочитать список
		/// Выводит список из JSON
		/// </summary>
		private async Task ReadJson()
		{
			var serializer = new JsonSerializer<int>();
			var array = await serializer.Read();
			Console.WriteLine($"Массив: {string.Join(", ", array)}");
		}

		/// <summary>
		/// Действие в меню Записать список
		/// Записывает JSON из введёного пользователем массива
		/// </summary>
		private async Task WriteJson()
		{
			var serializer = new JsonSerializer<int>();
			Console.Write("Введите массив: ");
			var array = Console.ReadLine()?
				.Split(' ', StringSplitOptions.RemoveEmptyEntries)
				.Where(item => int.TryParse(item, out _))
				.Select(int.Parse);
			var list = array!.ToList();
			Console.WriteLine($"Массив: {string.Join(", ", list)}");
			if (await serializer.Write(list))
			{
				Console.WriteLine("Массив сохранён");
			}
		}
	}
}