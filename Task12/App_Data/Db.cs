﻿using System.Configuration;
using LinqToDB;
using LinqToDB.Data;
using Task12.Models.Db;

namespace Task12.App_Data
{
	/// <summary>
	/// Подключение Linq2Db
	/// </summary>
	public class Db : DataConnection
	{
		/// <summary>
		/// Синглтон экземпляр.
		/// </summary>
		private static Db _instance;

		/// <summary>
		/// Предметы.
		/// </summary>
		public ITable<Subject> Subjects => GetTable<Subject>();

		/// <summary>
		/// Преподователи.
		/// </summary>
		public ITable<Teacher> Teachers => GetTable<Teacher>();

		/// <summary>
		/// Связь предметов и преподователей.
		/// </summary>
		public ITable<SubjectTeacherMap> SubjectTeachers => GetTable<SubjectTeacherMap>();

		/// <summary>
		/// Расписание..
		/// </summary>
		public ITable<Schedule> Schedules => GetTable<Schedule>();

		/// <summary>
		/// Группы.
		/// </summary>
		public ITable<Group> Groups => GetTable<Group>();

		/// <summary>
		/// Студенты.
		/// </summary>
		public ITable<Student> Students => GetTable<Student>();

		/// <summary>
		/// Создание подключения.
		/// </summary>
		/// <param name="connection">Строка подключения</param>
		private Db(string connection) : base(ProviderName.SqlServer2017, connection) { }

		/// <summary>
		/// Создание экземпляра.
		/// </summary>
		/// <returns>Экземпляра</returns>
		private static Db NewInstance()
		{
			var connectionString = ConfigurationManager.AppSettings.Get("connection");
			return new Db(connectionString);
		}

		/// <summary>
		/// Получить экземпляр.
		/// </summary>
		/// <returns>Экземпляр</returns>
		public static Db GetInstance()
		{
			if (_instance == null || _instance.Disposed)
			{
				_instance = NewInstance();
			}

			return _instance;
		}
	}
}
