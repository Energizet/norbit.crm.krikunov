﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using LinqToDB;
using Task12.App_Data;
using Task12.Models.View;

namespace Task12.Controllers
{
	/// <summary>
	/// CRUD студента
	/// </summary>
	public class StudentController : ApiController
	{
		/// <summary>
		/// Доступ в базу.
		/// </summary>
		private Db Db { get; set; }

		/// <summary>
		/// Запуск при каждом запросе
		/// </summary>
		protected override void Initialize(HttpControllerContext controllerContext)
		{
			base.Initialize(controllerContext);
			Db = Db.GetInstance();
		}

		// GET: api/Student
		/// <summary>
		/// Получить всех студентов
		/// </summary>
		/// <returns>Список студентов</returns>
		public async Task<IEnumerable<Student>> Get()
		{
			return await (from s in Db.Students
						  from g in Db.Groups.LeftJoin(g => g.Id == s.GroupId)
						  select new Student(s, g))
				.ToListAsync();
		}

		/// <summary>
		/// Получить студента
		/// </summary>
		/// <param name="id">Guid студента</param>
		/// <returns>Студент</returns>
		// GET: api/Student/5
		public async Task<Student> Get(Guid id)
		{
			return await (from s in Db.Students
						  from g in Db.Groups.LeftJoin(g => g.Id == s.GroupId)
						  where s.Id == id
						  select new Student(s, g))
				.FirstOrDefaultAsync();

		}

		// POST: api/Student
		/// <summary>
		/// Создать студента
		/// </summary>
		/// <param name="student">Студент</param>
		/// <returns>1 - если успешно</returns>
		public async Task<int> Post([FromBody] Student student)
		{
			if (student.Name == null)
			{
				throw new ArgumentException("Имя не может быть пустым");
			}

			if (student.Group != null)
			{
				var group = await Db.Groups
					.FirstOrDefaultAsync(g => g.Id == student.Group.Id);
				if (group == null)
				{
					throw new ArgumentException("Группа не найдена");
				}
			}

			if (student.Id != Guid.Empty)
			{
				throw new ArgumentException("Id указан. Для обновления используйте PUT");
			}
			
			return await Db.Students.InsertAsync(() => new Models.Db.Student
			{
				Id = Guid.NewGuid(),
				Name = student.Name,
				GroupId = student.Group != null ? student.Group.Id : (Guid?) null
			});
		}

		// PUT: api/Student/5
		/// <summary>
		/// Обновить студента (Не реализовано)
		/// </summary>
		/// <param name="id">Guid студента</param>
		/// <param name="student">Студент</param>
		/// <returns>1 - если успешно</returns>
		public async Task Put(Guid id, [FromBody] Student student)
		{
			throw new NotImplementedException("Не реализовано");
		}

		// DELETE: api/Student/5
		/// <summary>
		/// Удаление студента
		/// </summary>
		/// <param name="id">Guid студента</param>
		/// <returns>1 - если успешно</returns>
		public async Task<int> Delete(Guid id)
		{
			return await Db.Students
				.Where(s => s.Id == id)
				.DeleteAsync();
		}
	}
}
