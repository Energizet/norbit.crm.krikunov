﻿using System;
using LinqToDB.Mapping;

namespace Task12.Models.Db
{
	/// <summary>
	/// Студент.
	/// </summary>
	[Table]
	public class Student
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// ФИО
		/// </summary>
		[Column, NotNull]
		public string Name { get; set; }

		/// <summary>
		/// Id группы.
		/// </summary>
		[Column]
		public Guid? GroupId { get; set; }
	}
}
