﻿using System;
using LinqToDB.Mapping;

namespace Task12.Models.Db
{
	/// <summary>
	/// Группа.
	/// </summary>
	[Table]
	public class Group
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Название
		/// </summary>
		[Column, NotNull]
		public string Name { get; set; }
	}
}
