﻿using System;
using LinqToDB.Mapping;

namespace Task12.Models.Db
{
	/// <summary>
	/// Предмет
	/// </summary>
	[Table]
	public class Subject
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Название.
		/// </summary>
		[Column, NotNull]
		public string Name { get; set; }
	}
}
