﻿using System;
using LinqToDB.Mapping;

namespace Task12.Models.Db
{
	/// <summary>
	/// Учитель.
	/// </summary>
	[Table]
	public class Teacher
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// ФИО
		/// </summary>
		[Column, NotNull]
		public string Name { get; set; }
	}
}
