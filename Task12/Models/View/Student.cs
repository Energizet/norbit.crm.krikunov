﻿using System;
using System.Runtime.Serialization;
using LinqToDB.Mapping;

namespace Task12.Models.View
{
	/// <summary>
	/// Студент.
	/// </summary>
	[DataContract]
	[Table]
	public class Student
	{
		/// <summary>
		/// Id
		/// </summary>
		[DataMember]
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// ФИО
		/// </summary>
		[DataMember]
		[Column, NotNull]
		public string Name { get; set; }

		/// <summary>
		/// Группа.
		/// </summary>
		[DataMember]
		[Column]
		public Group Group { get; set; }

		/// <summary>
		/// Конструктор по-умолчанию.
		/// </summary>
		public Student() { }

		/// <summary>
		/// Создание студента view из db
		/// </summary>
		/// <param name="student">Студент из db</param>
		/// <param name="group">Группа из db</param>
		public Student(Db.Student student, Db.Group group)
		{
			Id = student.Id;
			Name = student.Name;
			if (group != null)
			{
				Group = new Group(group);
			}
		}
	}
}
