﻿using System;
using System.Runtime.Serialization;
using LinqToDB.Mapping;

namespace Task12.Models.View
{
	/// <summary>
	/// Группа.
	/// </summary>
	[DataContract]
	[Table]
	public class Group
	{
		/// <summary>
		/// Id
		/// </summary>
		[DataMember]
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Название
		/// </summary>
		[DataMember]
		[Column, NotNull]
		public string Name { get; set; }

		/// <summary>
		/// Конструктор по-уполчснию.
		/// </summary>
		public Group() { }

		/// <summary>
		/// Создание группы view из db
		/// </summary>
		/// <param name="group">Группа из db</param>
		public Group(Db.Group group)
		{
			Id = group.Id;
			Name = group.Name;
		}
	}
}
