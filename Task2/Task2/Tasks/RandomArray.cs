﻿using System;
using System.Collections.Generic;

namespace Norbit.Crm.Krikunov.Task2.Tasks
{
	/// <summary>
	/// Написать функцию, которая генерирует случайным образом
	/// элементы массива (число элементов в массиве и их тип
	/// определяются разработчиком), определяет для него максимальное
	/// и минимальное значения, сортирует массив и выводит полученный
	/// результат на экран. Примечание: LINQ запросы и готовые
	/// функции языка (Sort, Max и т.д.) использовать в данном
	/// задании запрещается.
	/// </summary>
	public class RandomArray
	{
		/// <summary>
		/// Управление вводом и выводом
		/// </summary>
		private readonly AbstractIo _io;

		/// <summary>
		/// Инициализация класса с управлением ввода и вывода
		/// </summary>
		/// <param name="io">Управляет вводом и выводом</param>
		private RandomArray(AbstractIo io)
		{
			_io = io;
		}

		/// <summary>
		/// Статический запуск задачи
		/// </summary>
		public static void Run(AbstractIo io)
		{
			var randomArray = new RandomArray(io);
			randomArray.Run();
		}

		/// <summary>
		/// Запуск задачи
		/// </summary>
		private void Run()
		{
			_io.WriteLine("=====Random Array=====");

			var random = new Random();

			var list = new List<int>();
			for (var i = 0; i < random.Next(3, 10); i++)
			{
				list.Add(random.Next(0, 100));
			}
			GC.Collect();

			_io.WriteLine("Исходный массив");
			_io.WriteLine(string.Join(", ", list));

			_io.WriteLine();
			_io.WriteLine("Максимальное число в массиве");
			_io.WriteLine(GetMax(list).ToString());

			_io.WriteLine();
			GetMin(list, out var min);
			_io.WriteLine("Минимальное число в массиве");
			_io.WriteLine(min.ToString());

			_io.WriteLine();
			var sortedList = Sort(list);
			_io.WriteLine("Сортированый массив");
			_io.WriteLine(string.Join(", ", sortedList));
		}

		/// <summary>
		/// Получение максимального числа из списка
		/// </summary>
		/// <param name="list">Список</param>
		/// <returns>Максимальное число</returns>
		private int GetMax(List<int> list)
		{
			var max = int.MinValue;
			foreach (var item in list)
			{
				if (item > max)
				{
					max = item;
				}
			}

			return max;
		}

		/// <summary>
		/// Получение минимального числа из списка
		/// </summary>
		/// <param name="list">Список</param>
		/// <param name="min">Минимальное число</param>
		private void GetMin(List<int> list, out int min)
		{
			min = int.MaxValue;
			foreach (var item in list)
			{
				if (item < min)
				{
					min = item;
				}
			}
		}

		/// <summary>
		/// Сортировка списка по возростанию
		/// </summary>
		/// <param name="list">Список</param>
		/// <returns>Отсортированный список</returns>
		private List<int> Sort(List<int> list)
		{
			var tmpList = new List<int>(list);
			for (var i = 0; i < tmpList.Count; i++)
			{
				for (var j = 1; j < tmpList.Count - i; j++)
				{
					if (tmpList[j - 1] > tmpList[j])
					{
						var tmpItem = tmpList[j];
						tmpList[j] = tmpList[j - 1];
						tmpList[j - 1] = tmpItem;
					}
				}
			}
			return tmpList;
		}
	}
}