﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Norbit.Crm.Krikunov.Task2.Tasks
{
	/// <summary>
	/// Вычисление средней длинны слов
	/// </summary>
	public static class AvgWords
	{
		/// <summary>
		/// Хранит лист слов из строки
		/// </summary>
		private readonly struct Words
		{
			/// <summary>
			/// Лист слов
			/// </summary>
			public List<string> WordList { get; }

			/// <summary>
			/// Получает массив слов из входной строки
			/// </summary>
			/// <param name="rawString">Входная строка</param>
			public Words(string rawString)
			{
				WordList = GetWordList(rawString);
			}
		}

		/// <summary>
		/// Запуск задачи
		/// </summary>
		public static void Run(AbstractIo io)
		{
			io.WriteLine("=====Avg Words=====");

			var words = new Words(io.ReadLine());
			io.WriteLine();
			io.WriteLine("Слова без знаков");
			io.WriteLine(string.Join(' ', words.WordList));
			io.WriteLine();
			io.WriteLine("Средняя длина слов");
			io.WriteLine(AvgLength(words.WordList).ToString("#.##", new NumberFormatInfo()));
		}
		
		/// <summary>
		/// Получает массив слов из входной строки
		/// </summary>
		/// <param name="str">Входная строка</param>
		/// <returns>Массив слов</returns>
		private static List<string> GetWordList(string str)
		{
			var words = new List<string>();
			var lastCharIsLetter = false;
			var lastWord = new StringBuilder();
			foreach (var letter in str)
			{
				var currentCharIsLetter = char.IsLetter(letter);
				if (currentCharIsLetter)
				{
					lastWord.Append(letter);
				}
				else if (lastCharIsLetter)
				{
					words.Add(lastWord.ToString());
					lastWord = new StringBuilder();
				}

				lastCharIsLetter = currentCharIsLetter;
			}
			words.Add(lastWord.ToString());

			return words;
		}

		/// <summary>
		/// Получает среднюю длину слов
		/// </summary>
		/// <param name="words">Слова</param>
		/// <returns>Средняя длина</returns>
		private static double AvgLength(List<string> words)
		{
			var sum = 0;
			foreach (var word in words)
			{
				sum += word.Length;
			}

			return (double)sum / words.Count;
		}
	}
}