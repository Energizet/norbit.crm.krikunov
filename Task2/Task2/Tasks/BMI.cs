﻿using System.Globalization;

namespace Norbit.Crm.Krikunov.Task2.Tasks
{
	/// <summary>
	/// Вычисление BMI
	/// </summary>
	public static class Bmi
	{
		/// <summary>
		/// Запуск задачи
		/// </summary>
		public static void Run(AbstractIo io)
		{
			io.WriteLine("=====BMI=====");

			io.WriteLine("Введите рост в см");
			var height = io.GetPositiveNumber();
			io.WriteLine("Введите вес в кг");
			var weight = io.GetPositiveNumber();
			var heightInMeters = height / 100f;
			var bmi = weight / heightInMeters / heightInMeters;

			io.WriteLine("Ваш BMI");
			io.WriteLine(bmi.ToString("#.##", new NumberFormatInfo()));
		}
	}
}