﻿using Norbit.Crm.Krikunov.Task2.Tasks;

namespace Norbit.Crm.Krikunov.Task2
{
	class Program
	{
		static void Main()
		{
			Bmi.Run(new ConsoleIo());
			RandomArray.Run(new ConsoleIo());
			AvgWords.Run(new ConsoleIo());
		}
	}
}
