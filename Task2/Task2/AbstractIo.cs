﻿using System;

namespace Norbit.Crm.Krikunov.Task2
{
	/// <summary>
	/// Управление вводом и выводом
	/// </summary>
	public abstract class AbstractIo
	{
		/// <summary>
		/// Колбек для цыкла
		/// </summary>
		/// <param name="outVar">Возврашаемое значение</param>
		/// <returns>true - если знаечение установленно</returns>
		protected delegate bool Callback<T>(out T outVar);

		/// <summary>
		/// Цыкл получения значения проверяемый колбеком
		/// </summary>
		/// <param name="action">Колбеком</param>
		protected T Loop<T>(Callback<T> action)
		{
			while (true)
			{
				try
				{
					var res = action(out var num);
					if (res)
					{
						return num;
					}
				}
				catch (Exception ex)
				{
					PrintException(ex);
				}
			}
		}

		/// <summary>
		/// Печатает исключение
		/// </summary>
		/// <param name="ex">Исключение</param>
		protected abstract void PrintException(Exception ex);

		/// <summary>
		/// Возвращает позитивное число
		/// </summary>
		/// <returns>Позитивное число</returns>
		public abstract int GetPositiveNumber();

		/// <summary>
		/// Читает строку
		/// </summary>
		public abstract string ReadLine();

		/// <summary>
		/// Печатает строку
		/// </summary>
		public abstract void WriteLine(string str = "");
	}
}