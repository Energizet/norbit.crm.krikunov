﻿using System;

namespace Norbit.Crm.Krikunov.Task2
{
	/// <summary>
	/// Управление вводом и выводом в консоль
	/// </summary>
	public class ConsoleIo : AbstractIo
	{

		/// <inheritdoc />
		public override int GetPositiveNumber()
		{
			return Loop(delegate (out int num)
			{
				var isCorrectInteger = int.TryParse(Console.ReadLine(), out num);
				if (!isCorrectInteger)
				{
					WriteLine("Введено не верное число");
					return false;
				}

				if (num < 1)
				{
					WriteLine("Число должно быть положительным");
					return false;
				}

				return true;
			});
		}

		/// <inheritdoc />
		public override string ReadLine()
		{
			return Console.ReadLine();
		}

		/// <inheritdoc />
		public override void WriteLine(string str = "")
		{
			Console.WriteLine(str);
		}

		/// <inheritdoc />
		protected override void PrintException(Exception ex)
		{
			Console.WriteLine(ex.ToString());
		}
	}
}