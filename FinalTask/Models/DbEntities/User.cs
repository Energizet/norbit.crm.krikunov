﻿using System;
using LinqToDB.Mapping;

namespace Norbit.Crm.Krikunov.FinalTask.Models.DbEntities
{
	/// <summary>
	/// Таблица пользователей.
	/// </summary>
	[Table]
	public class User
	{
		/// <summary>
		/// ID
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Доменное имя пользователя
		/// </summary>
		[Column, NotNull]
		public string UserDomainName { get; set; }
	}
}