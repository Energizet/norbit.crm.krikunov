﻿using System;
using LinqToDB.Mapping;
using Newtonsoft.Json;

namespace Norbit.Crm.Krikunov.FinalTask.Models.DbEntities
{
	/// <summary>
	/// Таблица Data.
	/// </summary>
	[Table]
	public class Data
	{
		/// <summary>
		/// Id
		/// </summary>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Id пользователя.
		/// </summary>
		[Column, NotNull]
		public Guid UserId { get; set; }

		/// <summary>
		/// Json сделки.
		/// </summary>
		[Column(Name = "Entity"), NotNull]
		public string EntityRaw { get; set; }

		/// <summary>
		/// Синглтон? десереализуемой сделки.
		/// </summary>
		private Entity _entity;

		/// <summary>
		/// Десереализация.
		/// </summary>
		public Entity Entity => _entity ?? (_entity = JsonConvert.DeserializeObject<Entity>(EntityRaw));
	}
}