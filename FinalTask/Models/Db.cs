﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;
using Norbit.Crm.Krikunov.FinalTask.Models.DbEntities;

namespace Norbit.Crm.Krikunov.FinalTask.Models
{
	/// <inheritdoc/>
	public class Db : IDb
	{
		/// <summary>
		/// Синглтон экземпляр.
		/// </summary>
		private static Db _instance;

		/// <summary>
		/// Подключение linq to db
		/// </summary>
		private readonly DbConnection _connection;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="connection">linq to db</param>
		public Db(DbConnection connection)
		{
			_connection = connection;
		}

		/// <inheritdoc/>
		public async Task<Entity> GetLastEntity()
		{
			var data = await (from d in _connection.Data
							  from u in _connection.User.InnerJoin(u => d.UserId == u.Id && u.UserDomainName == Environment.UserName)
							  select d).ToListAsync();

			var entities = from e in data
						   where e.Entity.Created <= DateTime.Now
						   orderby e.Entity.Created descending
						   select e;

			return entities.First().Entity;
		}

		/// <summary>
		/// Получить экземпляр.
		/// </summary>
		/// <returns>Экземпляр</returns>
		public static Db GetInstance()
		{
			if (_instance == null || DbConnection.IsDisposed)
			{
				_instance = new Db(DbConnection.GetInstance());
			}

			return _instance;
		}
	}
}