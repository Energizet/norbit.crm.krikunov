﻿using LinqToDB;
using LinqToDB.Data;
using Norbit.Crm.Krikunov.FinalTask.Models.DbEntities;

namespace Norbit.Crm.Krikunov.FinalTask.Models
{
	/// <summary>
	/// Linq2db
	/// </summary>
	public class DbConnection : DataConnection
	{
		/// <summary>
		/// Синглтон экземпляр.
		/// </summary>
		private static DbConnection _instance;

		public static bool IsDisposed => _instance == null || _instance.Disposed;

		/// <summary>
		/// Пользователи.
		/// </summary>
		public ITable<User> User => GetTable<User>();

		/// <summary>
		/// Данные.
		/// </summary>
		public ITable<Data> Data => GetTable<Data>();

		/// <summary>
		/// Создание подключения.
		/// </summary>
		/// <param name="connection">Строка подключения</param>
		private DbConnection(string connection) : base(ProviderName.SqlServer2017, connection) { }

		/// <summary>
		/// Создание экземпляра.
		/// </summary>
		/// <returns>Экземпляра</returns>
		private static DbConnection NewInstance() => new DbConnection(Environment.ConnectionString);

		/// <summary>
		/// Получить экземпляр.
		/// </summary>
		/// <returns>Экземпляр</returns>
		public static DbConnection GetInstance()
		{
			if (IsDisposed)
			{
				_instance = NewInstance();
			}

			return _instance;
		}
	}
}