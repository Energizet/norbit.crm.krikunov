﻿using System.Threading.Tasks;
using Norbit.Crm.Krikunov.FinalTask.Models.DbEntities;

namespace Norbit.Crm.Krikunov.FinalTask.Models
{
	/// <summary>
	/// Работа с базой.
	/// </summary>
	public interface IDb
	{
		/// <summary>
		/// Получить последнюю сделку
		/// </summary>
		/// <returns>Сделка</returns>
		Task<Entity> GetLastEntity();
	}
}