﻿using System.Configuration;
using System.Web;

namespace Norbit.Crm.Krikunov.FinalTask.Models
{
	/// <summary>
	/// Окружение.
	/// </summary>
	public static class Environment
	{
		/// <summary>
		/// Строка подключения к базе
		/// </summary>
		public static string ConnectionString => ConfigurationManager.AppSettings.Get("ConnectionString");

		/// <summary>
		/// Доступ к базе.
		/// </summary>
		public static Db Db => Db.GetInstance();

		/// <summary>
		/// Получение авторизировавшигося пользователя.
		/// </summary>
		public static string UserName => HttpContext.Current.User.Identity.Name;
	}
}