﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using Norbit.Crm.Krikunov.FinalTask.Models;
using Norbit.Crm.Krikunov.FinalTask.Models.DbEntities;

namespace Norbit.Crm.Krikunov.FinalTask.Controllers
{
	/// <summary>
	/// Апишка.
	/// </summary>
	public class EntityController : ApiController
	{
		/// <summary>
		/// Доступ в базу.
		/// </summary>
		private Db Db { get; set; }

		/// <summary>
		/// Запуск при каждом запросе
		/// </summary>
		protected override void Initialize(HttpControllerContext controllerContext)
		{
			base.Initialize(controllerContext);
			Db = Environment.Db;
		}

		// GET api/entity
		/// <summary>
		/// Получить последнюю сделку.
		/// </summary>
		public async Task<Entity> Get() => await Db.GetLastEntity();
	}
}
