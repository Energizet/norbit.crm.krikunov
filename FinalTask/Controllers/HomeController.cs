﻿using System.Web.Mvc;
using System.Web.Security;

namespace Norbit.Crm.Krikunov.FinalTask.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Title = "Home Page";

			return View();
		}

		/// <summary>
		/// Страница с вызовом через js
		/// </summary>
		public ActionResult EntityJs() => View();

		/// <summary>
		/// Страница с вызовом через c#
		/// </summary>
		public ActionResult EntityCs() => View();
	}
}
