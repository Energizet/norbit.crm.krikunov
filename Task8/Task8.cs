﻿using System;
using System.Globalization;
using Microsoft;

namespace Norbit.Crm.Krikunov.Task8
{
	/// <summary>
	/// Обернуть основные блоки кода в try\catch
	/// Реализовать вывод любой ошибки программы на консоль в Main, путем перехвата всех исключений в одном месте. При этом все внутренние исключения будут пробрасываться «наверх» как InnerException.
	/// Реализовать перевод даты из UTC в текущий часовой пояс пользователя.
	/// </summary>
	public static class Task8
	{
		/// <summary>
		/// Запуск задания
		/// </summary>
		public static void Run()
		{
			try
			{
				Console.WriteLine("Введите дату");
				Console.Write("(dd.MM.yyyy HH:mm): ");
				var str = Console.ReadLine();
				if (str == null)
				{
					throw new NullReferenceException("Ошибка ввода");
				}

				var dateTime = DateTime.ParseExact(str, "dd.MM.yyyy HH:mm", new DateTimeFormatInfo());
				var localDateTime = dateTime.ToLocalTime();
				Console.WriteLine(localDateTime.ToString("dd.MM.yyyy HH:mm"));
			}
			catch (FormatException ex)
			{
				throw new FormatException("Введён не верный формат", ex);
			}
			catch (Exception ex)
			{
				throw new Exception("Ошибка в задании 8", ex);
			}

		}
	}
}