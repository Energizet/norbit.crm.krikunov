﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Norbit.Crm.Krikunov.Libs;

namespace Norbit.Crm.Krikunov.Task8
{
	class Program
	{
		static void Main()
		{
			try
			{
				Task8.Run();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}
	}
}
