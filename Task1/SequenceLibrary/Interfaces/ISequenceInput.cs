﻿namespace Norbit.Crm.Krikunov.Task1.SequenceLibrary.Interfaces
{
	/// <summary>
	/// Получение данных для последовательности
	/// </summary>
	public interface ISequenceInput
	{
		/// <summary>
		/// Возвращает максимальное число последовательности
		/// </summary>
		/// <returns>Максимальное число</returns>
		public uint GetMaxNumber();
	}
}