﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Norbit.Crm.Krikunov.Task1.SequenceLibrary.Interfaces
{
	/// <summary>
	/// Вывод данных последовательности
	/// </summary>
	public interface ISequenceOutput
	{
		/// <summary>
		/// Записывает последовательность чисел
		/// </summary>
		/// <param name="sequence">Последовательность чисел</param>
		public void WriteSequence([NotNull] IEnumerable<int> sequence);
	}
}