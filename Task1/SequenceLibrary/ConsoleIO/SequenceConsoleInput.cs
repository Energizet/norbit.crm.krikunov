﻿using System;
using Norbit.Crm.Krikunov.Task1.SequenceLibrary.Interfaces;

namespace Norbit.Crm.Krikunov.Task1.SequenceLibrary.ConsoleIO
{
	/// <summary>
	/// Получение данных введимых пользователем в консоль
	/// </summary>
	public class SequenceConsoleInput : ISequenceInput
	{
		/// <summary>
		/// Колбек для цикла
		/// </summary>
		/// <param name="num">Возвращаемое число</param>
		/// <returns>true - если число установленно</returns>
		private delegate bool Callback(out uint num);

		/// <summary>
		/// Возвращает максимальное число последовательности введённое в консоль
		/// </summary>
		/// <inheritdoc />
		public uint GetMaxNumber()
		{
			return Loop(delegate (out uint num)
			{
				var isCorrectInteger = uint.TryParse(Console.ReadLine(), out num);
				if (!isCorrectInteger)
				{
					Console.WriteLine("Введено не верное число");
					return false;
				}

				if (num < 1)
				{
					Console.WriteLine("Число должно быть больше 1");
					return false;
				}
				return true;
			});
		}

		/// <summary>
		/// В цикле выполняет колбек
		/// </summary>
		/// <param name="action">Колбек</param>
		private uint Loop(Callback action)
		{
			while (true)
			{
				try
				{
					var res = action(out var num);
					if (res)
					{
						return num;
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
		}
	}
}