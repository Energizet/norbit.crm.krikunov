﻿using System.Collections.Generic;
using System.Linq;
using Microsoft;
using Norbit.Crm.Krikunov.Task1.SequenceLibrary.Interfaces;
using Output = System.Console;

namespace Norbit.Crm.Krikunov.Task1.SequenceLibrary.ConsoleIO
{
	/// <summary>
	/// Выводит данные в консоль
	/// </summary>
	public class SequenceConsoleOutput : ISequenceOutput
	{
		/// <summary>
		/// Выводит последовательность в консоль
		/// </summary>
		/// <param name="sequence">Последовательность</param>
		public void WriteSequence(IEnumerable<int> sequence)
		{
			sequence = Requires.NotNull(sequence, nameof(sequence));
			var stringSequence = sequence
				.Select(item => item.ToString())
				.Aggregate((l, r) => $"{l}, {r}");
			Output.WriteLine($"{stringSequence}.");
		}
	}
}