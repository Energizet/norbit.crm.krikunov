﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft;
using Norbit.Crm.Krikunov.Task1.SequenceLibrary.Interfaces;

namespace Norbit.Crm.Krikunov.Task1.SequenceLibrary
{
	/// <summary>
	/// Получает максимальное число и
	/// выводит последовательность от 1 до максимального числа
	/// </summary>
	public class NumericalSequence
	{
		/// <summary>
		/// Откуда получать данные для последовательности
		/// </summary>
		private readonly ISequenceInput _input;
		/// <summary>
		/// Куда выводить последовательность
		/// </summary>
		private readonly ISequenceOutput _output;

		/// <summary>
		/// Получает источник ввода и вывода
		/// </summary>
		/// <param name="input">Источник ввода: откуда получить данные</param>
		/// <param name="output">Источник вывода: куда данные вывести</param>
		public NumericalSequence([NotNull] ISequenceInput input, [NotNull] ISequenceOutput output)
		{
			Requires.NotNull(input, nameof(input));
			Requires.NotNull(output, nameof(output));
			_input = input;
			_output = output;
		}

		/// <summary>
		/// Точка запуска задачи
		/// </summary>
		public void StartTask()
		{
			var maxNumber = _input.GetMaxNumber();
			var sequence = GenerateSequence(maxNumber);
			_output.WriteSequence(sequence);
		}

		/// <summary>
		/// Генератор последовательности
		/// </summary>
		/// <param name="maxNumber">Максимальное число последовательности</param>
		/// <returns>Числа последовательности</returns>
		private IEnumerable<int> GenerateSequence(uint maxNumber)
		{
			for (var i = 1; i <= maxNumber; i++)
			{
				yield return i;
			}
		}
	}
}