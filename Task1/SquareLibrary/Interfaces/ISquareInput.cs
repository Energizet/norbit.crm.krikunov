﻿namespace Norbit.Crm.Krikunov.Task1.SquareLibrary.Interfaces
{
	/// <summary>
	/// Ввод данных для квадрата
	/// </summary>
	public interface ISquareInput
	{
		/// <summary>
		/// Возвращает не чётное число
		/// </summary>
		/// <returns>Не чётное число</returns>
		public uint GetOddNumber();
	}
}