﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Norbit.Crm.Krikunov.Task1.SquareLibrary.Interfaces
{
	/// <summary>
	/// Вывод данных квадрата
	/// </summary>
	public interface ISquareOutput
	{
		/// <summary>
		/// Выводит фигуру
		/// </summary>
		/// <param name="shape">Фигура</param>
		public void PrintShape([NotNull] IEnumerable<string> shape);
	}
}