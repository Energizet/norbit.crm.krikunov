﻿using System;
using Norbit.Crm.Krikunov.Task1.SquareLibrary.Interfaces;

namespace Norbit.Crm.Krikunov.Task1.SquareLibrary.ConsoleIO
{
	/// <summary>
	/// Получение данных введимых пользователем в консоль
	/// </summary>
	public class SquareConsoleInput : ISquareInput
	{
		/// <summary>
		/// Колбек для цикла
		/// </summary>
		/// <param name="num">Возвращаемое число</param>
		/// <returns>true - если число установленно</returns>
		private delegate bool Callback(out uint num);

		/// <summary>
		/// Возвращает не чётное число введённое в консоль
		/// </summary>
		/// <returns>Не чётное число</returns>
		public uint GetOddNumber()
		{
			return Loop(delegate (out uint num)
			{

				var isCorrectInteger = uint.TryParse(Console.ReadLine(), out num);
				if (!isCorrectInteger)
				{
					Console.WriteLine("Введено не верное число");
					return false;
				}

				if (num % 2 == 0)
				{
					Console.WriteLine("Число должно быть не чётным");
					return false;
				}

				return true;
			});
		}

		/// <summary>
		/// В цикле выполняет колбек
		/// </summary>
		/// <param name="action">Колбек</param>
		private uint Loop(Callback action)
		{
			while (true)
			{
				try
				{
					var res = action(out var num);
					if (res)
					{
						return num;
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
		}
	}
}