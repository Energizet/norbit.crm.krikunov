﻿using System.Collections.Generic;
using Microsoft;
using Norbit.Crm.Krikunov.Task1.SquareLibrary.Interfaces;
using Output = System.Console;

namespace Norbit.Crm.Krikunov.Task1.SquareLibrary.ConsoleIO
{
	/// <summary>
	/// Выводит данные в консоль
	/// </summary>
	public class SquareConsoleOutput : ISquareOutput
	{
		/// <summary>
		/// Выводит фигуру в консоль
		/// </summary>
		/// <inheritdoc />
		public void PrintShape(IEnumerable<string> shape)
		{
			shape = Requires.NotNull(shape, nameof(shape));
			foreach (var line in shape)
			{
				Output.WriteLine(line);
			}
		}
	}
}