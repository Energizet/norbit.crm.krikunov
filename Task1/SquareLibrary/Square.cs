﻿using System;
using System.Collections.Generic;
using System.Text;
using JetBrains.Annotations;
using Microsoft;
using Norbit.Crm.Krikunov.Task1.SquareLibrary.Interfaces;

namespace Norbit.Crm.Krikunov.Task1.SquareLibrary
{
	/// <summary>
	/// Получает размер квадрата и
	/// выводит квадрат из звёздочек с пробелом в центре
	/// </summary>
	public class Square
	{
		/// <summary>
		/// Откуда получать данные для последовательности
		/// </summary>
		private readonly ISquareInput _input;
		/// <summary>
		/// Куда выводить последовательность
		/// </summary>
		private readonly ISquareOutput _output;

		/// <summary>
		/// Получает источник ввода и вывода
		/// </summary>
		/// <param name="input">Источник ввода: откуда получить данные</param>
		/// <param name="output">Источник вывода: куда данные вывести</param>
		public Square([NotNull] ISquareInput input, [NotNull] ISquareOutput output)
		{
			Requires.NotNull(input, nameof(input));
			Requires.NotNull(output, nameof(output));
			_input = input;
			_output = output;
		}

		/// <summary>
		/// Точка запуска задачи
		/// </summary>
		public void StartTask()
		{
			var num = _input.GetOddNumber();
			var shape = GenerateSquare(num);
			_output.PrintShape(shape);
		}

		/// <summary>
		/// Генератор квадрата
		/// </summary>
		/// <param name="number">Размер квадрата</param>
		/// <returns>Построчно выводит квадрат</returns>
		private IEnumerable<string> GenerateSquare(uint number)
		{
			var numberInt = Convert.ToInt32(number);
			for (var i = 0; i < numberInt; i++)
			{
				var simblsBySide = numberInt / 2;
				var lineBuilder = new StringBuilder();
				lineBuilder.Append(new string('*', simblsBySide));
				lineBuilder.Append(numberInt / 2 == i ? " " : "*");
				lineBuilder.Append(new string('*', simblsBySide));
				yield return lineBuilder.ToString();
			}
		}
	}
}