﻿using System;
using Norbit.Crm.Krikunov.Task1.SequenceLibrary;
using Norbit.Crm.Krikunov.Task1.SequenceLibrary.ConsoleIO;
using Norbit.Crm.Krikunov.Task1.SquareLibrary;
using Norbit.Crm.Krikunov.Task1.SquareLibrary.ConsoleIO;

namespace Norbit.Crm.Krikunov.Task1.FirstDay
{
	class Program
	{
		static void Main()
		{
			Console.WriteLine("=====Sequence=====");

			var sequenceInput = new SequenceConsoleInput();
			var sequenceOutput = new SequenceConsoleOutput();
			var numericalSequence = new NumericalSequence(sequenceInput, sequenceOutput);
			numericalSequence.StartTask();

			Console.WriteLine("=====Square=====");

			var squareInput = new SquareConsoleInput();
			var squareOutput = new SquareConsoleOutput();
			var square = new Square(squareInput, squareOutput);
			square.StartTask();
		}
	}
}
